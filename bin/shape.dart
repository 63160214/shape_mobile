import 'package:shape/shape.dart' as shape;

void main(List<String> arguments) {
  Rectangle rect = new Rectangle(6,3);
  Triangle triangle = new Triangle(21,6);
  Circle circle = new Circle(4);
  Square square = new Square(4);

  square.area();
  circle.area();
  rect.area();
  triangle.area();
}


abstract class Shape {
  late int heigth;
  late int width;
  void area();

}

class Rectangle extends Shape {
 Rectangle(int a, int b){
  heigth = a;
  width = b;
 }
  @override
  void area() {
    print("Area of rectangle is ${heigth * width}");
  }
}

class Triangle extends Shape {
 Triangle(int a, int b){
  heigth = a;
  width = b;
 }
  @override
  void area() {
    print("Area of triangle is ${heigth * width}");
  }
}

class Circle extends Shape {
 Circle(int a){
  heigth = a;
  
 }
  @override
  void area() {
    print("Area of circle is ${3.14 * heigth * heigth}");
  }
}

class Square extends Shape {
 Square(int a){
  width = a;
 }
  @override
  void area() {
    print("Area of square is ${width * width}");
  }
}